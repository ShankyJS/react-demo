FROM node:10.15.1-jessie as react-build
MAINTAINER shankyjs<hello@shankyj.me>: 0.1

RUN mkdir /build
WORKDIR /build
COPY /demoreact /build
RUN npm install
RUN npm run build

#stage: 2
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build build/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
